import unittest
from unittest.mock import patch
import requests  # Импортируем библиотеку requests
from mass_get_requests import send_request  # Импортируем функцию из файла mass_get_requests.py

class TestSendRequest(unittest.TestCase):
    @patch('requests.get')
    def test_successful_request(self, mock_get):
        """Тест успешного GET-запроса, возвращающего статус 200."""
        mock_get.return_value.status_code = 200
        url = 'http://example.com'
        result = send_request(url)
        self.assertEqual(result, 200)

    @patch('requests.get')
    def test_request_exception(self, mock_get):
        """Тест обработки исключений при GET-запросе."""
        mock_get.side_effect = requests.RequestException("An error occurred")
        url = 'http://example.com'
        result = send_request(url)
        self.assertIsInstance(result, str)
        self.assertEqual(result, "An error occurred")

if __name__ == '__main__':
    unittest.main()


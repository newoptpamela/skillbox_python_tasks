import requests
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor

def send_request(url):
    """Функция для отправки GET-запроса к указанному URL."""
    try:
        response = requests.get(url)
        return response.status_code  # Возвращаем код состояния для проверки успешности запроса
    except requests.RequestException as e:
        return str(e)  # В случае ошибки возвращаем ее описание

def main():
    url = input("Введите URL-адрес ресурса: ")  # Запрашиваем URL у пользователя
    number_of_requests = 1000  # Количество запросов

    # Создаем ThreadPoolExecutor для асинхронной отправки запросов
    with ThreadPoolExecutor(max_workers=100) as executor:
        # Создаем progress bar с помощью tqdm
        with tqdm(total=number_of_requests, desc="Отправка запросов") as progress:
            # Создаем и отправляем запросы
            futures = [executor.submit(send_request, url) for _ in range(number_of_requests)]
            for future in futures:
                result = future.result()  # Получаем результат выполнения запроса
                progress.update(1)  # Обновляем progress bar

if __name__ == "__main__":
    main()

import unittest
from red_white_car_sort import arrange_cars  # Убедитесь, что функция находится в файле red_white_car_sort.py

class TestArrangeCars(unittest.TestCase):
    def test_more_red_than_white_plus_one(self):
        """Тест случая, когда красных машин больше, чем белых + 1."""
        self.assertEqual(arrange_cars(3, 1), 'Нет решения', "Expected 'Нет решения' when red > white + 1")

    def test_more_white_than_red_plus_one(self):
        """Тест случая, когда белых машин больше, чем красных + 1."""
        self.assertEqual(arrange_cars(1, 3), 'Нет решения', "Expected 'Нет решения' when white > red + 1")

    def test_equal_number_of_red_and_white(self):
        """Тест случая, когда красных и белых машин поровну."""
        self.assertIn(arrange_cars(2, 2), ['RWRW', 'WRWR'], "Expected valid alternating sequence when counts are equal")

    def test_no_red_cars(self):
        """Тест случая, когда нет красных машин."""
        self.assertEqual(arrange_cars(0, 3), 'WWW', "Expected 'WWW' when there are no red cars")

    def test_no_white_cars(self):
        """Тест случая, когда нет белых машин."""
        self.assertEqual(arrange_cars(3, 0), 'RRR', "Expected 'RRR' when there are no white cars")

    def test_single_red_car(self):
        """Тест случая, когда есть одна красная машина."""
        self.assertEqual(arrange_cars(1, 1), 'RW', "Expected 'RW' when there is one red and one white car")

    def test_single_white_car(self):
        """Тест случая, когда есть одна белая машина."""
        # Проверяем, что при наличии на одну красную машину больше, чередование корректно
        self.assertEqual(arrange_cars(2, 1), 'RWR', "Expected 'RWR' when there are more reds than whites by one")

if __name__ == '__main__':
    unittest.main()

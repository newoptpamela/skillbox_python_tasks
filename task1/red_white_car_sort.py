def arrange_cars(red, white):
    if red == 0:
        return 'W' * white
    if white == 0:
        return 'R' * red

    if abs(red - white) > 1:
        return 'Нет решения'

    result = []
    while red > 0 and white > 0:
        if red > white:
            result.append('R')
            red -= 1
            if white > 0:  # Убедимся, что белая машина добавляется, если она доступна
                result.append('W')
                white -= 1
        elif white > red:
            result.append('W')
            white -= 1
            if red > 0:  # Убедимся, что красная машина добавляется, если она доступна
                result.append('R')
                red -= 1
        elif red == white:  # Если количества равны, начинаем с красной
            result.append('R')
            red -= 1
            if white > 0:
                result.append('W')
                white -= 1

    # Добавляем оставшиеся машины (в этой версии они не должны появляться)
    result.extend(['R'] * red)
    result.extend(['W'] * white)

    return ''.join(result)
